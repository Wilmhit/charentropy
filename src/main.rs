use clap::{App, Arg};
use std::collections::HashMap;
use std::fs;
use std::str;

fn main() {
    let matches = get_cli_arguments();

    if let Some(filename) = matches.value_of("FILENAME") {
        let character = matches.value_of("character");
        let all = matches.is_present("all");
        let filename = filename.to_string();

        match (character, all) {
            (Some(character), false) => calculate_for_character(character.to_string(), filename),
            (Some(_), true) => println!(
                "You cannot calculate both for individual character and for all characters"
            ),
            (None, true) => calculate_for_all(filename),
            (None, false) => calculate_for_text(filename),
        }
    }
}

fn get_cli_arguments() -> clap::ArgMatches<'static> {
    let filename = Arg::with_name("FILENAME")
        .help("File to process")
        .required(true)
        .index(1);

    let character = Arg::with_name("character")
        .help("Character to calculate entropy for")
        .short("c")
        .long("character")
        .takes_value(true);

    let all = Arg::with_name("all")
        .help("Same as running with -c but multiple times for all characters")
        .short("a")
        .long("all");

    let app = App::new("Char Entropy")
        .version("0.2")
        .author("Char Entropy Team")
        .about("Calculates entropy of a character in given text")
        .arg(character)
        .arg(all)
        .arg(filename);

    return app.get_matches();
}

fn calculate_for_text(filename: String) {
    let text = fs::read_to_string(filename).unwrap();
    let text_bytes = text.as_bytes();
    let text_length = text.len() as u64;
    let counted = count_occurances(text_bytes);
    let entropy = total_entropy(&counted, text_length);

    for character in counted.iter() {
        let prob = propability_for_character(&counted, character.0, text_length);
        let char_str = u8_to_string(character.0);
        println!("P({}) = {}", char_str, prob);
    }

    println!("Total entropy: {}", entropy);
}

fn calculate_for_character(character: String, filename: String) {
    let text = fs::read_to_string(filename).unwrap();
    let text_bytes = text.as_bytes();
    let text_length = text.len() as u64;
    let char_byte = string_to_u8(character);
    let counted = count_occurances(text_bytes);

    let individual_entropy = entropy_for_character(&counted, &char_byte, text_length);
    let entropy = total_entropy(&counted, text_length);
    let individual_probability = propability_for_character(&counted, &char_byte, text_length);

    println!("Character: {}", u8_to_string(&char_byte));
    println!("Occured: {} times", counted.get(&char_byte).unwrap());
    println!("Individual entropy: {}", individual_entropy);
    println!("Individual probability: {}", individual_probability);
    println!("Total entropy: {}", entropy);
}

fn calculate_for_all(filename: String) {
    let text = fs::read_to_string(filename).unwrap();
    let text_bytes = text.as_bytes();
    let text_length = text.len() as u64;
    let counted = count_occurances(text_bytes);
    let entropy = total_entropy(&counted, text_length);

    for (char_byte, occurances) in counted.iter() {
        let individual_probability = propability_for_character(&counted, &char_byte, text_length);
        let individual_entropy = entropy_for_character(&counted, &char_byte, text_length);

        println!("===================================");
        println!("Character: {}", u8_to_string(&char_byte));
        println!("Occured: {} times", occurances);
        println!("Individual entropy: {}", individual_entropy);
        println!("Individual probability: {}", individual_probability);
        println!("Total entropy: {}", entropy);
    }
}

fn u8_to_string(character: &u8) -> String {
    let u8_array = [*character];
    let char_str = str::from_utf8(&u8_array);
    match char_str {
        Ok(result) => return String::from(result),
        Err(_) => return format!("ASCII:{}", *character),
    }
}

fn string_to_u8(character: String) -> u8 {
    //TODO would be usefull to detect numeric values for a nonprintable characters
    if character.len() != 1 {
        panic!("Provided character is not a single byte");
    }
    character.to_string().as_bytes()[0]
}

fn count_occurances(text_bytes: &[u8]) -> HashMap<u8, i32> {
    //Key in map is character (one byte) and value is total occurances
    let mut char_map: HashMap<u8, i32> = HashMap::new();
    for character in text_bytes.iter() {
        let count = char_map.entry(*character).or_insert(0);
        *count += 1;
    }
    char_map
}

fn propability_for_character(char_map: &HashMap<u8, i32>, char_byte: &u8, length: u64) -> f64 {
    let up = *char_map.get(char_byte).or(Some(&0)).unwrap() as f64;
    up / length as f64
}

fn entropy_for_character(char_map: &HashMap<u8, i32>, char_byte: &u8, length: u64) -> f64 {
    let probability = propability_for_character(char_map, char_byte, length);
    //log2 because binary digit can store 2 values
    let logged = probability.log2();
    probability * logged * -1.0
}

fn total_entropy(char_map: &HashMap<u8, i32>, length: u64) -> f64 {
    let mut total: f64 = 0.0;
    for character in char_map.iter() {
        total += entropy_for_character(char_map, character.0, length);
    }
    total
}
