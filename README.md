# Character entropy

This cli utility lets you calculate binary entropy for given text

## Usage

```
char_entropy [-c character] <FILENAME> 
```

`FILENAME` is the filename of given `.txt` file. Other types of files will work, unless contain unprintable characters.

`-c` flag lets you get detailed information for a single character.

## Installing from source

You need to have `cargo` on your PC. You can get it either from your distribution's repository or from [rustup.rs](https://rustup.rs/).
Then while in this directory do.
```
cargo install --path .
```
Now you have compiled and installed char_entropy binary. Try `char_entropy --help`.

